import React, {Component} from 'react';
import './miniCalculator.less';
import Button from "./Button";

class MiniCalculator extends Component {
  constructor(props){
    super(props);
    this.state = {
      result: 0
    }
    this.calculate = this.calculate.bind(this);
  }

  calculate(operator, num){
    switch (operator) {
      case '+': {this.state.result += parseInt(num);break;}
      case '-': {this.state.result -= parseInt(num);break;}
      case '*': {this.state.result *= parseInt(num);break;}
      case '/': {this.state.result /= parseInt(num);break;}
      default: break;
    }
    this.setState({result: this.state.result})
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.result} </span>
        </div>
        <div className="operations">
          <Button onClickFunc={this.calculate} operator={'+'} num={"1"} name={'加1'} />
          <Button onClickFunc={this.calculate} operator={'-'} num={"1"} name={'减1'} />
          <Button onClickFunc={this.calculate} operator={'*'} num={"2"} name={'乘以2'} />
          <Button onClickFunc={this.calculate} operator={'/'} num={"2"} name={'除以2'} />
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

