import React, {Component} from 'react';

class Button extends Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button onClick={() => this.props.onClickFunc(this.props.operator, this.props.num)}>{this.props.name}</button>
    );
  }

}

export default Button;